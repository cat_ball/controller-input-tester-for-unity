﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleButton : InputGUI {
	public Toggle toggle;
	
	void Start(){
		toggle = GetComponent<Toggle> ();
	}
	
	void Update(){
		toggle.isOn = Input.GetButton (inputName);
	}
}
