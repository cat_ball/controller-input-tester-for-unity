﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class JoystickText : InputGUI {
	public Text label;
	
	void Start(){
		label = GetComponent<Text> ();
	}
	
	void Update(){
		label.text = inputName + ": " + Input.GetAxis (inputName);
	}
}
