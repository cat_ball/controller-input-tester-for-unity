﻿using UnityEngine;
using System.Collections;


public class InputNameIsObjectName : MonoBehaviour {
	void Awake(){
		InputGUI myInput = GetComponent<InputGUI> ();
		if (myInput != null) {
			myInput.inputName = gameObject.name;
		}
	}
}
