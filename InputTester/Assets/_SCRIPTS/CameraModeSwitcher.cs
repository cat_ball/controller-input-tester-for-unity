﻿using UnityEngine;
using System.Collections;

public class CameraModeSwitcher : MonoBehaviour {
	public enum CameraMode {
		DESKTOP,
		GEAR_VR,
		GOOGLE_CARDBOARD
	}
	public CameraMode currentCameraMode = CameraMode.GEAR_VR;
	
	public GameObject gearCamera;
	public GameObject cardboardCamera;
	
	void Start(){
		gearCamera.SetActive (currentCameraMode == CameraMode.GEAR_VR || currentCameraMode == CameraMode.DESKTOP);
		cardboardCamera.SetActive (currentCameraMode == CameraMode.GOOGLE_CARDBOARD);
	}
}
