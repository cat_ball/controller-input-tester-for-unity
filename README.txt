For Gear VR Mode:
	Place your oculussig file in Plugins -> Android -> assets
	Build APK using ControllerTester Scene only
	Ensure "Virtual Reality Supported" IS enabled
		To find this option: Build Settings -> Player Settings -> Other Settings
	>>Note: Only tested using the Note 4 Gear VR
	
For Cardboard Mode:
	Build APK using ControllerTesterCardboard Scene only
	Ensure "Virtual Reality Supported" is NOT enabled
		To find this option: Build Settings -> Player Settings -> Other Settings
	>>Note: Only tested using Note 4 and Nexus 5 (so Android devices)
	

After building this application, it will tell you which input is being used
for a particular button or joystick, such as joystick button 0 or X Axis.

Then in the editor for your project, go to Edit -> Project Settings -> Input. 
Here you name the "axes" for your controller to use in code. The name of the 
axis is what you will put in scripts to detect if a button is pressed 
(ex, Input.GetButton("Button 0") or Input.GetAxis("X Axis")). 
	For buttons, type the name shown in this application as "joystick button #" in the Positive Button field (without "")
	For joysticks, change the type dropdown to Joystick Axis and Axis dropdown to the one shown in this application

These settings are stored in InputManager.asset in your project folder.

Mapped Inputs.txt has a list of input settings already mapped for some controllers.